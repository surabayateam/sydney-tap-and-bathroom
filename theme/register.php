<?php
//Define Current Directory
define("CURRENT_DIR",dirname(__FILE__));
//Autoload Register Current File
spl_autoload_register(function ($class) {
    $file_name=preg_replace("/(\\\\\\\\)+'/","/",$class);
    $path=CURRENT_DIR."\\".$file_name.".php";
    if(file_exists($path)){
        if(!class_exists($class)){
            require($path);
        }
    }
});

new core\post\Main();