<?php
/**
 * Created by PhpStorm.
 * User: TWMG
 * Date: 4/27/2018
 * Time: 4:08 PM
 */

namespace core\post;


class Main{
    public $template_file;
    public function __construct(){
        $this->template_file=dirname(__FILE__)."/template/post_loop.php";
        $this->showPost();
    }

    public function showPost(){
        include_once($this->template_file);
    }
}